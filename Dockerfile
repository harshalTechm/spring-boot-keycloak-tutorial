# Start with a base image containing Java runtime
FROM openjdk:8-jre-alpine

# Add Maintainer Info
MAINTAINER CRST

# Add a volume pointing to /tmp
VOLUME /tmp

# Make port 8082 available to the world outside this container
EXPOSE 8082

# The application's jar file
ARG JAR_FILE=target/product-app-0.0.1-SNAPSHOT.jar

# Add the application's jar to the container
ADD ${JAR_FILE} product-app-0.0.1-SNAPSHOT.jar

# Run the jar file
ENTRYPOINT ["java","-jar","/product-app-0.0.1-SNAPSHOT.jar"]